from fastapi import FastAPI, Path, Query, HTTPException, status
from typing import Optional
from pydantic import BaseModel

app = FastAPI()


class Item(BaseModel):
    name: str
    price: float
    brand: Optional[str] = None


class UpdateItem(BaseModel):
    name: Optional[str] = None
    price: Optional[float] = None
    brand: Optional[str] = None


inventory = {}


# Endpoint with path param
@app.get("/get-item/{item_id}")
def get_item(item_id: int = Path(None, description="The item id", gt=0)):
    if item_id in inventory:
        return inventory[item_id]
    else:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Item id does not exist")


# Endpoint with query param
@app.get("/get-by-name")
def get_item(name: str = Query(None, title="Name", description="Name of item")):
    for item_id in inventory:
        if inventory[item_id].name == name:
            return inventory[item_id]
        else:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Item name does not exist")


@app.post("/create-item/{item_id}")
def create_item(item_id: int, item: Item):
    if item_id in inventory:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Item id already exists")

    inventory[item_id] = item
    return inventory[item_id]


@app.put("/update-item/{item_id}")
def update_item(item_id: int, item: UpdateItem):
    if item_id in inventory:
        if item.name is not None:
            inventory[item_id].name = item.name
        if item.price is not None:
            inventory[item_id].price = item.price
        if item.brand is not None:
            inventory[item_id].brand = item.brand
    else:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Item id does not exist")
    return inventory[item_id]


@app.delete("/delete-item/{item_id}")
def delete_item(item_id: int):
    if item_id in inventory:
        del inventory[item_id]
    else:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Item id does not exist")
    return {"Success": "Item deleted"}
